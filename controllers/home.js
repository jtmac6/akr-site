/**
 * GET /
 * Home page.
 */
exports.index = (req, res) => {
  res.render('home', {
    title: 'Home'
  });
};

exports.aikido = (req, res) => {
  res.render('aikido', {
    title: 'About Aikido Kokikai'
  });
};

exports.about = (req, res) => {
  res.render('about', {
    title: 'About Us'
  });
};

exports.faq = (req, res) => {
  res.render('faq', {
    title: 'FAQ'
  });
};

exports.classes = (req, res) => {
  res.render('classes', {
    title: 'Classes'
  });
};

exports.dues = (req, res) => {
  res.render('dues', {
    title: 'Pay Dues'
  });
};

exports.instructors = (req, res) => {
  res.render('instructors', {
    title: 'Instructors'
  });
};

exports.reviews = (req, res) => {
  res.render('reviews', {
    title: 'Reviews'
  });
};
