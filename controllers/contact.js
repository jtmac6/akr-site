const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  service: 'SendGrid',
  auth: {
    user: process.env.SENDGRID_USER,
    pass: process.env.SENDGRID_PASSWORD
  }
});

/**
 * GET /contact
 * Contact form page.
 */
exports.getContact = (req, res) => {
  // const unknownUser = !(req.user);
  const unknownUser = true;
  res.render('contact', {
    title: 'Contact',
    unknownUser,
    captcha: res.recaptcha
  });
};

/**
 * POST /contact
 * Send a contact form via Nodemailer.
 */
exports.postContact = (req, res) => {
  if(!req.recaptcha.error){
    let fromName;
    let fromEmail;
    // if (!req.user) {
    //   req.assert('name', 'Name cannot be blank').notEmpty();
    //   req.assert('email', 'Email is not valid').isEmail();
    // }
    req.assert('name', 'Name cannot be blank').notEmpty();
    req.assert('email', 'Email is not valid').isEmail();
    req.assert('message', 'Message cannot be blank').notEmpty();

    const errors = req.validationErrors();

    if (errors) {
      req.flash('errors', errors);
      return res.redirect('/contact');
    }

    fromName = req.body.name;
    fromEmail = req.body.email;
    // if (!req.user) {
    //   fromName = req.body.name;
    //   fromEmail = req.body.email;
    // } else {
    //   fromName = req.user.profile.name || '';
    //   fromEmail = req.user.email;
    // }

    const mailOptions = {
      to: 'aikidoroc@gmail.com',
      from: `${fromName} <${fromEmail}>`,
      subject: 'Contact Form | Aikido Kokikai Rochester',
      text: req.body.message
    };

    transporter.sendMail(mailOptions, (err) => {
      if (err) {
        req.flash('errors', { msg: err.message });
        return res.redirect('/contact');
      }
      req.flash('success', { msg: 'Email has been sent successfully!' });
      res.redirect('/contact');
    });
  }else{
    req.flash('errors', { msg: 'Invalid Captcha' });
    return res.redirect('/contact');
  }
};
