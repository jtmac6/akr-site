(function() { 
    Galleria.loadTheme('https://cdnjs.cloudflare.com/ajax/libs/galleria/1.5.7/themes/classic/galleria.classic.min.js');
    Galleria.run('.galleria',{
        autoplay: 5000,
        imageCrop: true,
        lightbox: true,
        height: 500
    });
}());
